import React from 'react';

function TableItem({ data }) {
  return (
    <>
      {
        <tr>
          <td>{data.sign}</td>
          <td>{data.unicode_symbol}</td>
          <td>{data.dates}</td>
        </tr>
      }
    </>
  );
}

export default TableItem;