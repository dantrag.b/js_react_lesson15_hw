import React from 'react';
import data from '../../data';
import TableItem from '../tableItem/tableItem';


function Table() {
  return (
    <table className='zodiac__table'>
      <thead>
        <tr>
          <th>Sign</th>
          <th>Pic</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody>
        {
          data.map(element => {
            return <TableItem data={element} key={element.sign}></TableItem>
          })
        }
      </tbody>
    </table >
  );
}

export default Table;