const data = [
  {
    "sign": "Aries",
    "dates": "Mar 21 - Apr 19",
    "unicode_symbol": "♈"
  },
  {
    "sign": "Taurus",
    "dates": "Apr 20 - May 20",
    "unicode_symbol": "♉"
  },
  {
    "sign": "Gemini",
    "dates": "May 21 - Jun 21",
    "unicode_symbol": "♊"
  },
  {
    "sign": "Cancer",
    "dates": "Jun 22 - Jul 22",
    "unicode_symbol": "♋"
  },
  {
    "sign": "Leo",
    "dates": "Jul 23 - Aug 22",
    "unicode_symbol": "♌"
  },
  {
    "sign": "Virgo",
    "dates": "Aug 23 - Sep 22",
    "unicode_symbol": "♍"
  },
  {
    "sign": "Libra",
    "dates": "Sep 23 - Oct 22",
    "unicode_symbol": "♎"
  },
  {
    "sign": "Scorpio",
    "dates": "Oct 23 - Nov 21",
    "unicode_symbol": "♏"
  },
  {
    "sign": "Sagittarius",
    "dates": "Nov 22 - Dec 21",
    "unicode_symbol": "♐"
  },
  {
    "sign": "Capricorn",
    "dates": "Dec 22 - Jan 19",
    "unicode_symbol": "♑"
  },
  {
    "sign": "Aquarius",
    "dates": "Jan 20 - Feb 18",
    "unicode_symbol": "♒"
  },
  {
    "sign": "Pisces",
    "dates": "Feb 19 - Mar 20",
    "unicode_symbol": "♓"
  }
];

export default data;