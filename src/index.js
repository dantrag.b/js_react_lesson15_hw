import React from 'react';
import ReactDOM from 'react-dom/client';
import Table from './components/table/table';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Table></Table>
);
